package com.example.cooking_helper;

public class LowItem {
	private String Image;
	private String Name;
	private String Describe;
	private String Link;
	
	public LowItem() {
		//do nothing
	}
	
	public LowItem(String image, String name,String describe, String link) {
		super();
		this.Image=image;
		this.Name=name;
		Describe = describe;
		this.Link = link;
	}
	
	
	//name
	public String getName() {
		return Name;
	}
	
	public void setName(String name) {
		Name = name;
	}
	
	//image
	
	public String getImage() {
		return Image;
	}
	public void setDescribe(String name) {
		Describe = name;
	}
	
	//image
	
	public String getDescribe() {
		return Describe;
	}
	
	public void setLink(String name) {
		Link = name;
	}
	
	
	public String getLink() {
		return Link;
	}
	
}
