package com.example.cooking_helper;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.R.color;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.MonthDisplayHelper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.example.main_adapter.GridViewAdapter;
import com.example.main_adapter.ItemGridView;

public class MainActivity extends Activity {
	private GridView myGridView;
	private SearchView searchView;
	private GridViewAdapter myAdapter;
	private ArrayList<ItemGridView> itemArray;
	private Fragment1 newFragment1;
	private ArrayList<LowItem> myArray;
	private FragmentTransaction ft;
	boolean inMain = true;
	String monAnName;
	Button back;
	ImageButton home;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		String[] name = { "Món Kho", "Món Xào", "Món Xốt", "Món Om",
				"Món Rán-Chiên-Quay", "Món Nướng", "Món Rang", "Món Rô ti",
				"Món Lẩu", "Món Hấp-Luộc", "Món Tái", "Món Gỏi", "Món Nộm",
				"Món Soup-Canh-Hầm", "Món Nem-Chả", "Món Cuốn", "Món Kim Chi",
				"Món Salad", "Nước Chấm", "Món Cháo", "Món Chè", "Món Mứt",
				"Món Kem", "Thức uống-Sinh tố", "Món Chay", "Món Bánh",
				"Cách chế biến khác" };
		final String[] link = { "http://congthucmonngon.com/category/cach-che-bien-2/mon-kho", "http://congthucmonngon.com/category/cach-che-bien-2/mon-xao", "http://congthucmonngon.com/category/cach-che-bien-2/mon-xot-sot",
				"http://congthucmonngon.com/category/cach-che-bien-2/mon-om", "http://congthucmonngon.com/category/cach-che-bien-2/mon-ran-chien-quay", "http://congthucmonngon.com/category/cach-che-bien-2/mon-nuong", 
				"http://congthucmonngon.com/category/cach-che-bien-2/mon-rang", "http://congthucmonngon.com/category/cach-che-bien-2/mon-ro-ti", "http://congthucmonngon.com/category/cach-che-bien-2/mon-lau",
				"http://congthucmonngon.com/category/cach-che-bien-2/mon-hap-luoc", "http://congthucmonngon.com/category/cach-che-bien-2/mon-tai", "http://congthucmonngon.com/category/cach-che-bien-2/mon-goi", 
				"http://congthucmonngon.com/category/cach-che-bien-2/mon-nom", "http://congthucmonngon.com/category/cach-che-bien-2/mon-soup-canh-ham","http://congthucmonngon.com/category/cach-che-bien-2/mon-nem-cha", 
				"http://congthucmonngon.com/category/cach-che-bien-2/mon-cuon", "http://congthucmonngon.com/category/cach-che-bien-2/mon-kim-chi", "http://congthucmonngon.com/category/cach-che-bien-2/mon-salad", 
				"http://congthucmonngon.com/category/cach-che-bien-2/nuoc-cham", "http://congthucmonngon.com/category/cach-che-bien-2/mon-chao", "http://congthucmonngon.com/category/cach-che-bien-2/mon-che",
				"http://congthucmonngon.com/category/cach-che-bien-2/nuoc-mut","http://congthucmonngon.com/category/cach-che-bien-2/mon-kem", "http://congthucmonngon.com/category/cach-che-bien-2/thuc-uong-sinh-to", 
				"http://congthucmonngon.com/category/cach-che-bien-2/mon-chay", "http://congthucmonngon.com/category/cach-che-bien-2/mon-banh", "http://congthucmonngon.com/category/cach-che-bien-2/cach-che-bien-khac" };
		int[] imageId = { R.drawable.mon_kho, R.drawable.mon_xao,
				R.drawable.mon_xot, R.drawable.mon_om,
				R.drawable.mon_ran, R.drawable.mon_nuong,
				R.drawable.mon_rang, R.drawable.mon_roti,
				R.drawable.mon_lau, R.drawable.mon_hapluoc,
				R.drawable.mon_tai, R.drawable.mon_goi,
				R.drawable.mon_nom, R.drawable.mon_canh,
				R.drawable.mon_nem, R.drawable.mon_cuon,
				R.drawable.mon_kimchi, R.drawable.mon_salad,
				R.drawable.nuoc_cham, R.drawable.mon_chao,
				R.drawable.mon_che, R.drawable.mon_mut,
				R.drawable.mon_kem, R.drawable.sinh_to,
				R.drawable.mon_chay, R.drawable.mon_banh,
				R.drawable.chebien_khac};
		//
		searchView = (SearchView) findViewById(R.id.search_view);
		searchView.setBackgroundColor(color.white);
		myGridView = (GridView) findViewById(R.id.gridViewFood);
		back = (Button) findViewById(R.id.back);
		home = (ImageButton) findViewById(R.id.home);
		itemArray = new ArrayList<ItemGridView>();
		for (int i = 0; i < 27; i++) {
			ItemGridView a = new ItemGridView(name[i], link[i], imageId[i]);
			itemArray.add(a);
		}
		myAdapter = new GridViewAdapter(getApplicationContext(),
				R.layout.item_grid_view, itemArray);
		myGridView.setAdapter(myAdapter);
		searchView.setSelected(false);
		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				Log.d("hi", " " + query);
				inMain = false;
				monAnName = query;
				NameSpace.setMonanName(monAnName);
				FragmentManager manager = getFragmentManager();
				FragmentTransaction transition = manager.beginTransaction();
				ListFood frangment = new ListFood();
				transition.replace(R.id.tabContent, frangment);
				transition.addToBackStack(null);
				transition.commit();
				return false;				
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		
		home.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
		
		myGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				NameSpace.setListLink(link[position]);
				FragmentManager manager = getFragmentManager();
				FragmentTransaction transition = manager.beginTransaction();
				Fragment1 frangment = new Fragment1();
				transition.replace(R.id.tabContent, frangment);
				transition.addToBackStack(null);
				transition.commit();
			}
		});
	}

}
