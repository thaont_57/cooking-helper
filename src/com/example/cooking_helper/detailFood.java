package com.example.cooking_helper;

import java.io.InputStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class detailFood extends Fragment {
	ProgressDialog mProgressDialog;
	String id;
	String html;
	View rootview;
	WebView myWebview;
	String name;
	String monanName;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootview = inflater.inflate(R.layout.detail_food, container, false);
		myWebview = (WebView) rootview.findViewById(R.id.mywebview);
		id = NameSpace.getMonanLink();
//		myWebview.setOnTouchListener(new View.OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				return true;
//			}
//		});

		new CreateDetail().execute();
		return rootview;

	}

	private class CreateDetail extends AsyncTask<Void, Void, String> {
		//boolean error = false;

		@Override
		protected String doInBackground(Void... params) {
			getInfo();
			return html;
		}

		@Override
		protected void onPostExecute(String result) {
			setInfo();
			mProgressDialog.dismiss();

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(getActivity());
			// Set progressdialog title
			//mProgressDialog.setTitle(monanName);
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		private void getInfo() {
			try {
				String getlink = NameSpace.getMonanLink();
				Document connect = Jsoup.connect(getlink).get();
				Elements name = connect.select("h1[class=post-title item fn]");
				monanName = name.text();
				Log.d("name", monanName+"hi");
				Elements a = connect.select("div[class=post-content-right]");
				Log.d("html", a.html());
				html ="<h1> <b>"+name.html()+"</b></h1>"+"<br>"+ a.html();
				html=html.substring(0, html.indexOf("<div class=\"pvc"));
				html=html.replace("http://congthucmonngon.com/tag/", " ");

			} catch (Exception e) {
				Log.d("error", e.getMessage());
			}

		}

		private void setInfo() {
			myWebview.loadDataWithBaseURL(null, html, "text/html", "utf-8",
					null);
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			if (ImageAdapter.containskey(urls[0]))
				return ImageAdapter.getImage(urls[0]);
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
				ImageAdapter.put(urls[0], mIcon11);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}

	}

}
