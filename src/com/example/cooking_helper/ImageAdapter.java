package com.example.cooking_helper;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;

public class ImageAdapter {
	public static Map<String, Bitmap> drawableMap = new HashMap<String, Bitmap>();
	public static WeakReference<Map<String, Bitmap>> mymemory
	= new WeakReference<Map<String, Bitmap>>(drawableMap);
	
	
	public static boolean containskey(String key) {
		return mymemory.get().containsKey(key);
		//return false;
	}
	
	public static Bitmap getImage(String key) {
		return mymemory.get().get(key);
	}
	public static void put(String key, Bitmap image) {
		mymemory.get().put(key, image);
	}

}
