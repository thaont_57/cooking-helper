package com.example.cooking_helper;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class Fragment1 extends Fragment{
	View rootview;
	private myAdapter myAdap;
	private ListView myList;
	private ArrayList<LowItem> myArray;
	ProgressDialog dialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootview = inflater.inflate(R.layout.list_food, container, false);
		myList = (ListView) rootview.findViewById(R.id.mylist);	
		getview();
		myList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String index = myArray.get(position).getLink();
				NameSpace.setMonanLink(index);
				FragmentManager manager = getFragmentManager();
				FragmentTransaction transition = manager.beginTransaction();
				detailFood frangment = new detailFood();
				transition.replace(R.id.tabContent, frangment);
				transition.addToBackStack(null);
				transition.commit();
			}
			
		});
		return  rootview;
	}

	private void getview() {
		myArray = new ArrayList<LowItem>();
        myAdap= new myAdapter(getActivity().getApplicationContext(), R.layout.low_item, myArray);
        myList.setAdapter(myAdap);
        new DownloadListFoods(myArray).execute();
	}
	
    private class DownloadListFoods extends AsyncTask<String, Void, Void> {
    	
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Loading...");
			dialog.setIndeterminate(false);
			dialog.show();
		}
//
		public DownloadListFoods(ArrayList<LowItem> MyArray) {
			myArray=MyArray;
		}
//
		protected Void doInBackground(String... urls) {
			String getlink = NameSpace.getListLink();
			Log.d("link", getlink);
			String select = "div[class=column half]";
			try {
				Document doc = Jsoup.connect(getlink).get();
				Elements a = doc.select(select);
				Elements Describes = a.select("div[class=excerpt]");
				Elements NguyenLieus = a.select("h2[itemprop=name]").select("a");
				Elements Images = a.select("a").select("img");
				for (int i=0;i<NguyenLieus.size();i++) {
					String link = NguyenLieus.get(i).attr("href");
					String name = NguyenLieus.get(i).attr("title");
					String image = Images.get(i).attr("src");
					String describe = Describes.get(i).select("p").text();
					describe = describe.substring(0,describe.indexOf("�"));
					Log.d("des",describe);
					LowItem b = new LowItem(image,name,describe,link);
					myArray.add(b);
				}
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(Void a) {
			myAdap.notifyDataSetChanged();
			dialog.dismiss();
		}
		
	}

}
