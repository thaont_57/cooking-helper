package com.example.cooking_helper;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.example.main_adapter.ItemGridView;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ListFood extends Fragment {
		
		View rootView;
		private myAdapter myAdap;
		private ListView myList;
		private ArrayList<LowItem> myArray;
		ProgressDialog dialog;
		String search;		
		
		public ListFood() {
			search=NameSpace.getMonanName();
		}

		@Override 
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			rootView = inflater.inflate(R.layout.list_food, container, false);
			myList = (ListView) rootView.findViewById(R.id.mylist);	
			getview();
			myList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					String index = myArray.get(position).getLink();
					NameSpace.setMonanLink(index);
					FragmentManager manager = getFragmentManager();
					FragmentTransaction transition = manager.beginTransaction();
					detailFood frangment = new detailFood();
					transition.replace(R.id.tabContent, frangment);
					transition.addToBackStack(null);
					transition.commit();
				}
			});
			return rootView;
		}
		
		
		private void getview(){
			myArray = new ArrayList<LowItem>();
				myAdap= new myAdapter(getActivity().getApplicationContext(), R.layout.low_item, myArray);
				myList.setAdapter(myAdap);
				new DownloadListFoods(myArray).execute(search);
			
		}
		
	    private class DownloadListFoods extends AsyncTask<String, Void, Void> {
	    	@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = new ProgressDialog(getActivity());
				dialog.setMessage("Loading List...");
				dialog.setIndeterminate(false);
				dialog.show();
			}
			//
			public DownloadListFoods(ArrayList<LowItem> MyArray) {
				myArray=MyArray;
			}
//
			protected Void doInBackground(String... urls) {
				urls[0] = NameSpace.getMonanName();
				String find=urls[0];
				find=find.replace(" ", "+");
				String select = "div[class=column half]";
				try {
					Document doc = Jsoup.connect("http://congthucmonngon.com/?s="+find).get();
					Elements a = doc.select(select);
					Elements Describes = a.select("div[class=excerpt]");
					Elements NguyenLieus = a.select("h2[itemprop=name]").select("a");
					Elements Images = a.select("a").select("img");
					for (int i=0;i<NguyenLieus.size();i++) {
						String link = NguyenLieus.get(i).attr("href");
						String name = NguyenLieus.get(i).attr("title");
						String image = Images.get(i).attr("src");
						String describe = Describes.get(i).select("p").text();
						describe = describe.substring(0,describe.indexOf("…"));
						LowItem b = new LowItem(image,name,describe,link);
						myArray.add(b);
					}
					if(myArray.isEmpty()){
						FragmentManager manager = getFragmentManager();
						FragmentTransaction transition = manager.beginTransaction();
						noFound frangment = new noFound();
						transition.replace(R.id.tabContent, frangment);
						transition.addToBackStack(null);
						transition.commit();
					}
				} catch (Exception e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(Void a) {
				myAdap.notifyDataSetChanged();
				dialog.dismiss();
			}
		}
}