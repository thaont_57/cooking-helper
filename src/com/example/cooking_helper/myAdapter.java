package com.example.cooking_helper;

import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class myAdapter extends ArrayAdapter<LowItem> {
	ArrayList<LowItem> itemList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public myAdapter(Context context, int textViewResourceId,
			ArrayList<LowItem> objects) {
		super(context, textViewResourceId, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = textViewResourceId;
		itemList = objects;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.ImageView1);
			holder.nameview = (TextView) v.findViewById(R.id.name);
			holder.describeView = (TextView) v.findViewById(R.id.describe);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		holder.nameview.setText(itemList.get(position).getName());
		holder.describeView.setText(itemList.get(position).getDescribe());
		new DownloadImageTask(holder.imageview).execute(itemList.get(
				position).getImage());
		return v;
		
		
//		if (ImageAdapter.containskey(itemList.get(position).getImage())) {
//			Log.d("map in adapter", "already have in memory " + position);
//			holder.imageview.setImageBitmap(ImageAdapter.getImage(itemList.get(
//					position).getImage()));
//			holder.nameview.setText(itemList.get(position).getName());
//			return v;
//		} else {
//			holder.imageview.setImageResource(R.drawable.loading);
//			new DownloadImageTask(holder.imageview).execute(itemList.get(
//					position).getImage());
//			holder.nameview.setText(itemList.get(position).getName());
//			return v;
//		}

	}
	
	static class ViewHolder {
		public ImageView imageview;
		public TextView nameview;
		public TextView describeView;

	}
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {

			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}

	}


	

}

