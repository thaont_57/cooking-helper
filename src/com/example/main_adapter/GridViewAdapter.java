package com.example.main_adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cooking_helper.LowItem;
import com.example.cooking_helper.R;

public class GridViewAdapter extends ArrayAdapter<ItemGridView> {
	ArrayList<ItemGridView> itemList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public GridViewAdapter(Context context, int resource, ArrayList<ItemGridView> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		itemList = objects;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.ImageView1);
			holder.nameview = (TextView) v.findViewById(R.id.name);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.imageview.setImageResource(itemList.get(position).getImageID());
		holder.nameview.setText(itemList.get(position).getName());
		return v;

	}
	
	static class ViewHolder {
		public ImageView imageview;
		public TextView nameview;

	}
}
