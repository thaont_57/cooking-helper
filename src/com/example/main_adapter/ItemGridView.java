package com.example.main_adapter;

public class ItemGridView {
	String Name;
	String Link;
	int ImageID;
	
	public ItemGridView(String name, String link, int id){
		Link = link;
		Name = name;
		ImageID = id;
	}
	
	public String getName(){
		return Name;
	}
	
	public String getLink(){
		return Link;
	}
	
	public int getImageID(){
		return ImageID;
	}

}
